import {
  HANDLE_MODAL,
  UPDATE_MERCHANT_STATE_SUCCESS,
  UPDATE_MERCHANT_STATE_FAILURE,
  READY_TO_EDIT,
  EDIT_DONE,
  READY_TO_DELETE,
  DELETE_DONE,
  SELECT_AVATAR,
  SET_VIEW_BID_VAL,
  INIT_AVATAR_STATE,
  REFRESH_AVATAR_STATE,
  CRUD_ACTION_FAILED
} from './actionType';

import {FETCH_CALL, ADD_CALL, UPDATE_CALL, DELETE_CALL} from '../../helper/helper';

export const initAvatars = () => {
  return {
    type : INIT_AVATAR_STATE
  }
}

export const refreshAvatars = () => {
  return {
    type : REFRESH_AVATAR_STATE
  }
}

export const selectAvatar = (avatarName,avatarSrcValue) => {
  return {
    type : SELECT_AVATAR,
    avatarSrcValue : avatarSrcValue,
    avatarName : avatarName
  }
}

export const toggleModal = (params) => {
  return {
    type : HANDLE_MODAL,
    value : params
  }
}

export const updateMerchantStateSUCCESS = (params) => {
  return {
    type : UPDATE_MERCHANT_STATE_SUCCESS,
    value : params
  }
}

export const updateMerchantStateFAILURE = (params) => {
  return {
    type : UPDATE_MERCHANT_STATE_FAILURE,
    value : params
  }
}


export const editDone = () => {
  return {
    type : EDIT_DONE
  }
}


export const deleteDone = () => {
  return {
    type : DELETE_DONE
  }
}

export const crudActionFailed = (params) => {
  return {
    type : CRUD_ACTION_FAILED,
    value : params
  }
}
export const getReadyToEdit = (params) => {
  return {
    type : READY_TO_EDIT,
    value : params
  }
}

export const readyToDelete = (params) => {
  return {
    type : READY_TO_DELETE,
    value : params
  }
}

export const setViewBidVal = (params) => {
  return {
    type : SET_VIEW_BID_VAL,
    value : params
  }
}


export const getMerchants = () => {
  return (dispatch) => {
    FETCH_CALL().then( (response) => {
        dispatch(updateMerchantStateSUCCESS(response));
    }).catch((errorMsg)=>{
        dispatch(updateMerchantStateFAILURE(errorMsg));
    });
  };
}


export const deleteMerchantConfirm = (id) =>{
  return (dispatch) => {
    DELETE_CALL(id).then(
      () => {
         dispatch(getMerchants());
         dispatch(editDone());
      }
    ).catch(
      () => {
        dispatch(crudActionFailed("Merchant Could Not be Deleted"))
      }
    )
  }
}


export const addMerchant = (merchantObject) => {
  return (dispatch) => {
    ADD_CALL(merchantObject).then( () => {
        dispatch(getMerchants());
        dispatch(editDone()); // This is to clear the fields of addMerchantForm for next add operation once the user has confimed the add process
    }).catch(
      () => {
        dispatch(crudActionFailed("Merchant Cannot be added"));
      }
    );
  }
}

export const updateMerchant = (id,merchantObject) => {
    return (dispatch)=>{
      UPDATE_CALL(id,merchantObject).then( () => {
          dispatch(getMerchants());
          dispatch(editDone());
      }).catch(
        () => {
          dispatch(crudActionFailed("Merchant Cannot be Updated"))
        }
      );
    }
}
export const fetchbyIDfromAPIandSetToEditObj = (id,dispatch) => {
  return (dispatch) => {
    FETCH_CALL(id).then( (response) => {
      dispatch(getReadyToEdit(response));
    });
  }
}
