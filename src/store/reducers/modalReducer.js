const defaultState = {
  modalShow : {
    add : false,
    edit : false,
    delete : false,
    viewBids : false
  }
}


const modalReducer = (state=defaultState, action) => {
  switch (action.type) {
    case 'HANDLE_MODAL':
    let mShow = {};
    mShow[action.value] = !state.modalShow[action.value];
    return{
      ...state,
      modalShow:mShow
    }
    default:
    return state
  }
}

export default modalReducer;
