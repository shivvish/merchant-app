import {getIndexbyObjProperty} from '../../helper/helper';

const initAvatarState = {
  avatars : [
    {name: "avatar1", value:{src: "avatar/male1.svg", avtClass : "avatar"}},
    {name: "avatar2", value:{src: "avatar/male2.svg", avtClass : "avatar"}},
    {name: "avatar3", value:{src: "avatar/male3.svg", avtClass : "avatar"}},
    {name: "avatar4", value:{src: "avatar/male4.svg", avtClass : "avatar"}},
    {name: "avatar5", value:{src: "avatar/female1.svg", avtClass : "avatar"}},
    {name: "avatar6", value:{src: "avatar/female2.svg", avtClass : "avatar"}},
    {name: "avatar7", value:{src: "avatar/female3.svg", avtClass : "avatar"}},
    {name: "avatar8", value:{src: "avatar/female4.svg", avtClass : "avatar"}},
  ],
  avatarSelected : "/male1.svg",
  selectedIndex : ""
};


const avatarReducer = (state=initAvatarState, action) => {
  switch (action.type) {
    case 'INIT_AVATAR_STATE':
    return{
      ...state,
      avatars : state.avatars
    };
    case 'SELECT_AVATAR':
    let avatarsArray = state.avatars;
    let index = getIndexbyObjProperty(avatarsArray,"name",action.avatarName);
    let desiredObj = avatarsArray[index];
    desiredObj.value.avtClass = "avatar avatar-selected";
    return{
      ...state,
      avatars : avatarsArray.slice(),
      avatarSelected:action.avatarSrcValue
    };
    case 'REFRESH_AVATAR_STATE':
    let avatarsArr = state.avatars;
    for(let i in avatarsArr){
      avatarsArr[i]["value"]["avtClass"] = "avatar";
    }
    return{
      ...state,
      avatars : avatarsArr
    };
    default:
    return state
  }
}

export default avatarReducer;
