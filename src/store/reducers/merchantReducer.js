const defaultState = {
  allMerchants : [],
  fetchSuccess : false
}

const merchantReducer = (state=defaultState, action) => {
  switch (action.type) {
    case 'UPDATE_MERCHANT_STATE_SUCCESS':
    return{
      ...state,
      allMerchants: action.value,
      fetchSuccess : true
    };
    case 'UPDATE_MERCHANT_STATE_FAILURE':
    return{
      ...state,
      fetchSuccess: false
    };
    default:
    return state
  }
}

export default merchantReducer;
