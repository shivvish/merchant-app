const merchantTemplate = {
  "id": "",
  "firstname": "",
  "lastname": "",
  "avatarUrl": "",
  "email": "",
  "phone": "",
  "hasPremium": false
};

const defaultState = {
  readyToEditObj : merchantTemplate,
  readyToDeleteObj : {},
  viewBidVal : [],
  actionResult: {
    actionPerformed : false,
    actionSuccess : false,
    resultMsg : ''
  }
}



const formReducer = (state=defaultState, action) => {
  switch (action.type) {
    case 'READY_TO_EDIT':
    return{
      ...state,
      readyToEditObj:action.value
    };
    case 'EDIT_DONE':
    return{
      ...state,
      readyToEditObj:merchantTemplate
    };
    case 'READY_TO_DELETE':
    return{
      ...state,
      readyToDeleteObj:{
        id:action.value
      }
    };
    case 'DELETE_DONE':
    return{
      ...state,
      readyToDeleteObj:{}
    };
    case 'CRUD_ACTION_FAILED':
    return{
      ...state,
      actionResult: {
        actionPerformed : true,
        actionSuccess : false,
        resultMsg : action.value
      }
    };
    case 'SET_VIEW_BID_VAL':
    return{
      ...state,
      viewBidVal:action.value
    };
    default:
    return state
  }
}

export default formReducer;
