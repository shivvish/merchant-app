import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import {Provider} from 'react-redux';
import {createStore, applyMiddleware, combineReducers} from 'redux';
import thunk from 'redux-thunk';
//import reducer from './store/reducers/reducer';
import formReducer from './store/reducers/formReducer';
import merchantReducer from './store/reducers/merchantReducer';
import modalReducer from './store/reducers/modalReducer';
import avatarReducer from './store/reducers/avatarReducer';

const rootReducer = combineReducers({
  form_R : formReducer,
  merchant_R : merchantReducer,
  modal_R : modalReducer,
  avatar_R : avatarReducer
});

const store = createStore(rootReducer,applyMiddleware(thunk));

ReactDOM.render(<Provider store={store}><App /></Provider>, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
