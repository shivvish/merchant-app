import React, { Component } from 'react';
import './App.css';
import {Button, Modal} from 'react-bootstrap';
import  Merchants from './components/merchants';
import  AddOREditMerchant from './components/addOrEditMerchant';
import {connect} from 'react-redux';
import {toggleModal} from './store/actions/actionCreator';

class App extends Component {

  render() {
    const {toggleModal} = this.props;
    return (
      <div className="App">
      <div className="center">
      <h1>Merchant's List</h1>
      <Button bsStyle="primary" bsSize="sm" onClick={()=> toggleModal("add")}>
      Add a New Merchant
      </Button>
      </div>
      <Modal show={this.props.modalShow.add} onHide={()=> toggleModal("add")}>
      <Modal.Header closeButton>
      <Modal.Title className="center">Add a New Merchant</Modal.Title>
      </Modal.Header>
      <Modal.Body>
      <AddOREditMerchant/>
      </Modal.Body>
      <Modal.Footer>
      <Button onClick={()=> toggleModal("add")}>Close</Button>
      </Modal.Footer>
      </Modal>
      <Merchants/>
      </div>
    );
  }
}


const mapStatetoProps = (state) =>{
  return {
    modalShow:state.modal_R.modalShow
  }
}

const mapDispatchToProps = (dispatch) => {
  return{
    toggleModal : (modalType) => dispatch(toggleModal(modalType))
  }
}

export default connect(mapStatetoProps,mapDispatchToProps)(App);
