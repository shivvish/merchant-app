const base_url = 'http://localhost:3004/merchants/';

export const FETCH_CALL = (id='') => {
    return  fetch(base_url+'/'+id).then(response => response.json() ).then( (response_data) =>  {return response_data; });
}

export const ADD_CALL = (obj) => {
    return   fetch(base_url, {
          method: 'POST',
          headers: {
          'Content-Type': 'application/json'
          },
          body: JSON.stringify(obj)
        });
}

export const UPDATE_CALL = (id,obj) => {
  return fetch(base_url+id, {
    method: 'PATCH',
    headers: {
    'Content-Type': 'application/json'
    },
    body: JSON.stringify(obj)
  });
}

export const DELETE_CALL = (id) => {
    return   fetch(base_url+id, {
      method: 'DELETE',
      headers: {
        'Content-Type': 'application/json'
      }
    });
}


export const SORT_BIDS = (bidsArray,propertyName) => {
  return bidsArray.sort((a,b) => (getDateFromString(a[propertyName]) > getDateFromString(b[propertyName])) ? 1 : ((getDateFromString(b[propertyName]) > getDateFromString(a[propertyName])) ? -1 : 0))
}

const getDateFromString = (dateString) => {
  let formatDate = new Date(dateString);
  return formatDate;
}

export const getIndexbyObjProperty = (arrayofObj,propertyName,propertyValue) => {
  const index = arrayofObj.findIndex(item => item[propertyName] === propertyValue);
  return index;
}
