import React, {Component} from 'react';
import {connect} from 'react-redux';
import {getReadyToEdit} from '../store/actions/actionCreator';

class MerchantFormFields extends Component {

  chagedValue = (fieldName,e) => {
    const {readyToEditObj,getReadyToEdit} = this.props;
    let changedValue;
    let changedMerchant = Object.assign({},readyToEditObj);
    if(e.target.type === "checkbox"){
      changedValue = e.target.checked
    }else {
      changedValue = e.target.value;
    }
    changedMerchant[fieldName] = changedValue;
    getReadyToEdit(changedMerchant);
  }




  render(){
    const {readyToEditObj} = this.props;
    const {chagedValue} = this;
    return(
      <div>
      <input placeholder="FirstName" name="firstname" className="form-control" value={readyToEditObj.firstname} onChange={chagedValue.bind(this,'firstname')} required/><br/>
      <input placeholder="LastName" name="lastname" className="form-control" value={readyToEditObj.lastname} onChange={chagedValue.bind(this,'lastname')} required/><br/>
      <input placeholder="Phone" name="phone" className="form-control" value={readyToEditObj.phone}  onChange={chagedValue.bind(this,'phone')}required/><br/>
      <input placeholder="Email" name="email" type="email" className="form-control"value={readyToEditObj.email} onChange={chagedValue.bind(this,'email')} required/><br/>
      <input type="checkbox" name="hasPremium"  checked={!!readyToEditObj.hasPremium} onChange={chagedValue.bind(this,'hasPremium')} /> Premium Merchant<br/>
      <br/>
      </div>
    )
  }

}


const mapStateToProps = (state) => {
  return{
    readyToEditObj:state.form_R.readyToEditObj
  };
}

const mapDispatchToProps = (dispatch) => {
  return{
    getReadyToEdit : (obj) => dispatch(getReadyToEdit(obj))
  }
}
export default connect(mapStateToProps,mapDispatchToProps)(MerchantFormFields);
