import React, {Component} from 'react';
import AvatarDiv from './avatarDiv';
import MerchantFormFields from './form';
import {connect} from 'react-redux';
import {toggleModal,addMerchant} from '../store/actions/actionCreator';

class AddMerchant extends Component{
  addHandle = (e) => {
    e.preventDefault();
    const {toggleModal, addMerchant, readyToEditObj, avatarSelected} = this.props;
    let merchantObject = Object.assign({},readyToEditObj);
    merchantObject["avatarUrl"] = avatarSelected;
    merchantObject["bids"] = [];
    addMerchant(merchantObject);
    toggleModal("add");
  }



  render(){
    return(
      <div>
        <AvatarDiv/>
        <form onSubmit={this.addHandle.bind(this)}>
        <MerchantFormFields />
        <br/>
        <input type="submit" className="btn btn-success btn-block" value="Add Merchant"/>
        </form>
      </div>
    )
  }
}

const mapStatetoProps = (state) => {
  return{
    readyToEditObj:state.form_R.readyToEditObj,
    avatarSelected:state.avatar_R.avatarSelected
  }
}

const mapDispatchToProps = (dispatch) => {
  return{
    toggleModal : (modalType) => dispatch(toggleModal(modalType)),
    addMerchant : (merchantObject) => dispatch(addMerchant(merchantObject))
  }
}

export default connect(mapStatetoProps,mapDispatchToProps)(AddMerchant);
