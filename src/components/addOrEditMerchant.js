import React, {Component} from 'react';
import AddMerchant from './addmerchant';
import EditMerchant from './editmerchant';
import {connect} from 'react-redux';

class AddOREditMerchant extends Component {
  render(){
    const {modalShow} = this.props;
    if(modalShow.add){
      return(
        <AddMerchant/>
      );
    }else if (modalShow.edit) {
      return(
        <EditMerchant/>
      );
    }else {
      return(
        <div>
        Oops !
        </div>
      )
    }
  }
}

const mapStateToProps = (state) => {
  return{
    modalShow:state.modal_R.modalShow
  }
}

export default connect(mapStateToProps) (AddOREditMerchant);
