import React, {Component} from 'react';
import {Row, Col, Button, Modal} from 'react-bootstrap';
import {connect} from 'react-redux';
import  AddOREditMerchant from './addOrEditMerchant';
import ViewBids from './viewBids';
import {SORT_BIDS} from '../helper/helper';
import {toggleModal, readyToDelete, setViewBidVal, deleteMerchantConfirm, fetchbyIDfromAPIandSetToEditObj} from '../store/actions/actionCreator';

class Merchant extends Component{

  deleteMerchant = (id) => {
    const {toggleModal, readyToDelete} = this.props;
    readyToDelete(id);
    toggleModal("delete");
  }

  deleteMerchantConfirm = () => {
    const {toggleModal, readyToDeleteObj, deleteMerchantConfirmToAPI} = this.props;
    deleteMerchantConfirmToAPI(readyToDeleteObj.id);
    toggleModal("delete");
  }

  editMerchant = (id) => {
    const {toggleModal, fetchbyIDfromAPIandSetToEditObj} = this.props;
    toggleModal('edit');
    fetchbyIDfromAPIandSetToEditObj(id);
  }

  viewBids = (bids) => {
    const {toggleModal,setViewBidVal} = this.props;
    let sortedBids = SORT_BIDS(bids,'created'); // 1st Parameter : Array of Object , 2nd : Property of object over which array is to be sorted
    setViewBidVal(sortedBids); // setting Sorted Bids Array of object make it ready to display
    toggleModal("viewBids");
  }

  render(){
    const {merchantinfo,toggleModal} = this.props;
    const {viewBids,editMerchant,deleteMerchant,deleteMerchantConfirm} = this;
    return(
      <div>


      <Row className="show-grid merchant-rows" key={merchantinfo.id}>
      <Col md={2} >
      <div className="merchant-profile">
      <img alt="Avatar" className="avatar"  src={'/avatar'+merchantinfo.avatarUrl}/>
      </div>
      </Col>
      <Col md={5}>
      <h5>{merchantinfo.firstname}  {merchantinfo.lastname}</h5>
      <p><span className="info-icon"><img alt="email" src="/icon/contact-phone-icon.svg"/></span> &nbsp; {merchantinfo.phone}</p>
      <p><span className="info-icon"><img alt="email" src="/icon/email-icon.svg"/></span> &nbsp; {merchantinfo.email}</p>
      {merchantinfo.hasPremium?(<span className="alert-success premium-span">Premium</span>):(<span></span>)}
      <hr/>
      </Col>
      <Col md={5} className="center">
      <Button bsStyle="default" bsSize="sm" onClick={viewBids.bind(this,merchantinfo.bids)}>
      View Bids
      </Button>
      &nbsp;&nbsp;
      <Button bsStyle="info" bsSize="sm" onClick={editMerchant.bind(this,merchantinfo.id)}>
      Edit
      </Button>
      &nbsp;&nbsp;
      <Button bsStyle="danger" bsSize="sm" onClick={deleteMerchant.bind(this,merchantinfo.id)}>
      Delete
      </Button>
      </Col>
      </Row>

      <Modal show={this.props.modalShow.delete} onHide={()=>{toggleModal("delete")}}>
      <Modal.Header closeButton>
      <Modal.Title className="center">Delete Merchant</Modal.Title>
      </Modal.Header>
      <Modal.Body>
      <h2 className="center">Are You Sure ? </h2>
      </Modal.Body>
      <Modal.Footer>
      <Button bsStyle="success"  onClick={ deleteMerchantConfirm } >Confirm</Button>
      <Button onClick={()=>{toggleModal("delete")}} >Close</Button>
      </Modal.Footer>
      </Modal>

      <Modal show={this.props.modalShow.edit} onHide={()=>{toggleModal("edit")}}>
      <Modal.Header closeButton>
      <Modal.Title className="center">Edit Merchant</Modal.Title>
      </Modal.Header>
      <Modal.Body>
      <AddOREditMerchant />
      </Modal.Body>
      <Modal.Footer>
      <Button onClick={()=>{toggleModal("edit")}} >Close</Button>
      </Modal.Footer>
      </Modal>

      <Modal show={this.props.modalShow.viewBids} onHide={()=>{toggleModal("viewBids")}}>
      <Modal.Header closeButton>
      <Modal.Title className="center">Bidding History</Modal.Title>
      </Modal.Header>
      <Modal.Body>
      <ViewBids  />
      </Modal.Body>
      <Modal.Footer>
      <Button onClick={()=>{toggleModal("viewBids")}} >Close</Button>
      </Modal.Footer>
      </Modal>
      </div>
    )
  }
}



const mapStateToProps = (state) => {
  return{
    allMerchants:state.merchant_R.allMerchants,
    modalShow:state.modal_R.modalShow,
    readyToEditObj:state.form_R.readyToEditObj,
    readyToDeleteObj:state.form_R.readyToDeleteObj,
    viewBidVal:state.form_R.viewBidVal
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    toggleModal : (modalType) => dispatch(toggleModal(modalType)),
    readyToDelete : (id) => dispatch(readyToDelete(id)),
    setViewBidVal : (bids) => dispatch(setViewBidVal(bids)),
    deleteMerchantConfirmToAPI : (id) => dispatch(deleteMerchantConfirm(id)),
    fetchbyIDfromAPIandSetToEditObj : (id) => dispatch(fetchbyIDfromAPIandSetToEditObj(id))
  }
}

export default connect(mapStateToProps,mapDispatchToProps)(Merchant);
