import React, {Component} from 'react';
import MerchantFormFields from './form';
import {connect} from 'react-redux';
import {updateMerchant, toggleModal} from '../store/actions/actionCreator';

class EditMerchant extends Component{

  editHandle = (id,e) => {
    e.preventDefault();
    const {toggleModal, updateMerchant, readyToEditObj} = this.props;
    let merchantObject = Object.assign({},readyToEditObj);
    updateMerchant(id,merchantObject);
    toggleModal("edit");
  }


  render(){
    const {readyToEditObj} = this.props;
    const {editHandle} = this;
    return(
      <div>
      <div className="row center">
      <img alt="Avatar" className="avatar"  src={'/avatar'+readyToEditObj.avatarUrl}/>
      <p>{readyToEditObj.firstname}&nbsp;{readyToEditObj.lastname}</p>
      </div>

      <form onSubmit={editHandle.bind(this,readyToEditObj.id)}>
      <MerchantFormFields />
      <br/>
      <input type="submit" className="btn btn-success btn-block" value="Update"/>
      </form>
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return{
    readyToEditObj : state.form_R.readyToEditObj,
    avatarSelected : state.avatar_R.avatarSelected
  }
}

const mapDispatchToProps = (dispatch) => {
  return{
    toggleModal : (modalType) => dispatch(toggleModal(modalType)),
    updateMerchant : (id,merchantObject) => dispatch(updateMerchant(id,merchantObject))
  }
}

export default connect(mapStateToProps,mapDispatchToProps)(EditMerchant);
