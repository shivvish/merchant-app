import React from 'react';

const ApiResultDiv = (props) => {
  if(props.resultSuccess){
    return(
      <div className="alert alert-success center">
        Yeah ! Action Succes
        <hr/> {props.resultMsg}
      </div>
    )
  }else {
    return(
      <div className=" alert alert-danger center">
          Sorry ! Something Went Wrong
          <hr/> {props.resultMsg}
      </div>
    )
  }


}

export default ApiResultDiv;
