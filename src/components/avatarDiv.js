import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Row, Col} from 'react-bootstrap';
import {selectAvatar, initAvatars, refreshAvatars} from '../store/actions/actionCreator';


class AvatarDiv extends Component{


  changeAvatar = (e) =>{
    const {selectAvatar, refreshAvatars} = this.props;
    refreshAvatars();
    selectAvatar(e.target.name,e.target.src.replace(/(.+\w\/)(.+)/,"/$2"));
  }

  render(){
    const {changeAvatar} =this;
    const {avatars} = this.props;
    return(
      <div>
      <h5 className="center">Pick an Avatar</h5>
      <Row>
      <Col md={2}></Col>
        <Col md={8}>
        <Row>
          {avatars.map((avatar)=>{
            return(
              <Col md={3} key={avatar.name}>
                <img alt="avatar" className={avatar.value.avtClass} name={avatar.name}  src={avatar.value.src} onClick={changeAvatar.bind(this)} />
              </Col>
            )
          })}
        </Row>
        </Col>
      <Col md={2}></Col>
      </Row>

      </div>
    )
  }

  componentDidMount(){
    const {initAvatars} = this.props;
    initAvatars();
  }

}



const mapStateToProps = (state) => {
  return {
    avatars : state.avatar_R.avatars
  };
}

const mapDispatchToProps = (dispatch) => {
  return{
    initAvatars : () => dispatch(initAvatars()),
    refreshAvatars : () => dispatch(refreshAvatars()),
    selectAvatar : (avatarName,avatarSrcValue) => dispatch(selectAvatar(avatarName,avatarSrcValue))
  }
}

export default connect(mapStateToProps,mapDispatchToProps) (AvatarDiv);
