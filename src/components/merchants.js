import React, {Component} from 'react';
import {connect} from 'react-redux';
import Merchant from './merchant';
import {getMerchants} from '../store/actions/actionCreator';
import ApiResultDiv from './apiresultdiv';
import CRUDActionResultHandler from './crudactionresult';
class Merchants extends Component {
  render(){
    const {allMerchants, fetchSuccess, actionResult} = this.props;
    if(fetchSuccess){
      return(
        <div>
        <CRUDActionResultHandler actionResult={actionResult} />
        <hr/>
        {allMerchants.map((i)=>{
          return (
            <Merchant key={i.id} merchantinfo={i} />
          )
        })}
        </div>
      )
    }else {
      return(
          <ApiResultDiv resultSuccess={fetchSuccess} resultMsg="No Merchants Found"/>
      )
    }

  }

  componentDidMount(){
    const {getMerchants} = this.props;
    getMerchants();
  }

}

const mapStateToProps = (state) => {
  return{
    allMerchants:state.merchant_R.allMerchants,
    fetchSuccess:state.merchant_R.fetchSuccess,
    actionResult : state.form_R.actionResult,
    modalShow:state.modal_R.modalShow
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    getMerchants : () => dispatch(getMerchants())
  }
}

export default connect(mapStateToProps,mapDispatchToProps)(Merchants);
