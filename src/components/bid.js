import React, {Component} from 'react';

class Bid extends Component {
  render(){
    return(
      <div className="row bid-items center">
        <div className="col-md-6">
          {this.props.bid.carTitle}
        </div>
        <div className="col-md-6">
          {this.props.bid.created}
        </div>
      </div>
    )
  }
}


export default Bid;
