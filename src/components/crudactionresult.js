import React from 'react';
import ApiResultDiv from './apiresultdiv';
const CRUDActionResultHandler = (props) => {
  if(!props.actionResult.actionPerformed){
    return (
      <div>
      </div>
    )
  }else {
    return(
      <ApiResultDiv resultSuccess={props.actionResult.actionSuccess} resultMsg={props.actionResult.resultMsg}/>
    )
  }
}

export default CRUDActionResultHandler;
