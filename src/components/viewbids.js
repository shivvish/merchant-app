import React, {Component}  from 'react';
import {connect} from 'react-redux';
import Bid from './bid';
import {setViewBidVal} from '../store/actions/actionCreator';
class ViewBids extends Component {


  render(){
    const {viewBidVal} = this.props;
    if(viewBidVal.length>0){
      return(

        <div className="row">
        <div className="col-md-12" title="Bids Sorted by date-created">
        <h5 className="center">Merchant's Bidding Information</h5><br/>
        <div className="row bid-head center">
        <div className="col-md-6">
        Car Model
        </div>
        <div className="col-md-6">
        Created on
        </div>
        </div>
        {viewBidVal.map((bid)=>{
          return(<Bid key={bid.id} bid={bid}/>)
        })}
        </div>
        </div>
      )
    }else {
      return(
        <div className="row">
        <div className="col-md-12">
        <div className=" alert alert-danger">
        Sorry ! No Bids for this Merchant
        </div>

        </div>
        </div>
      )
    }
  }
}

const mapStateToProps = (state) => {
  return{
    viewBidVal:state.form_R.viewBidVal
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    setViewBidVal : (bid) => dispatch(setViewBidVal)
  }
}

export default connect(mapStateToProps,mapDispatchToProps)(ViewBids);
