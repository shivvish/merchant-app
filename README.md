#Simple React App

React based SPA which for managing merchants/bids.

  A user can :
    View a list of merchants.
    Add a merchant
    Edit a merchant
    Remove a merchant
    View history of bids created by each merchant(sorted by the date of creation).


Front-end part has been developed as SPA using
  ES6,
  React (react-redux, react-bootstrap).

All react components are class components


Back-end API is mocked using 'json-server'.

Setup:
    'npm-start' to get app started
    At ./src/ use 'json-server --watch db.json --port 3004' to get fake API get started.
